const Route = use('Route')

    Route.group(() => {
      //Auth routes
      Route.post('login', 'UserController.login')
      Route.post('signup', 'UserController.register')
      Route.get('checkall', 'UserController.checkUsers')
      Route.put('updateprofile', 'UserController.updateUser')


      //Game Lists Routes
      Route.get('games', 'GameController.index')
    }).prefix('api/v1')