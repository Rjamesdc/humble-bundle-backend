'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('token').nullable()
      table.string('image').nullable()
      table.string('background').nullable()
      table.string('fullname').nullable()
      table.string('address').nullable()
      table.string('mobilenumber').nullable()
      table.string('email').nullable()
      table.string('password').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UsersSchema
