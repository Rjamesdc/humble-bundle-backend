'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Game extends Model {
    static get table () {
        return 'games'
      }

      static get primaryKey () {
        return 'id'
      }
}

module.exports = Game
