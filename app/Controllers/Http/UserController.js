'use strict'

const { findBy } = require("../../Models/Game");

const User = use('App/Models/User');

class UserController {

    // simple login functionality, needs alot of validation such as checking of tokens.
    async login ({params,request, response}) {
        const userInfo = request.only(['email', 'password'])
        const user = await User.findBy({email: userInfo.email})
        if(!user || userInfo.email !== user.email || userInfo.password !== user.password) {
            return response.status(422).json({data: 'User or Password does not match.'}) 
           }
        console.log(user)
        return response.status(200).json({user})
    }

    // retrieve all user rows in table
    async checkUsers({params,request, response}){
        let users = await User.all()
        return response.json(users)
    }

    // update user through parameter token
    async updateUser({params,request,response}){
        const userInfo = request.only(['token','email', 'password', 'fullName', 'mobileNumber', 'image', 'background', 'address'])
        if(!userInfo.fullName || !userInfo.email 
           || !userInfo.token || !userInfo.password 
           || !userInfo.mobileNumber || !userInfo.image 
           || !userInfo.background || !userInfo.address) { return response.status(422).json({data: 'Please Fill up all fields' }) }
        const user = await User.findBy({token: userInfo.token})
            if(!user) return response.status(404).json({data: 'Resource not found'})

            user.title = userInfo.title
            user.email = userInfo.email
            user.fullname = userInfo.fullName
            user.password = userInfo.password
            user.mobilenumber = userInfo.mobileNumber
            user.background = userInfo.background
            user.image = userInfo.image
            user.address = userInfo.address

            await user.save()
            return response.status(200).json(user)
    }

    // session check but should use some kind of a token that expires (JTW or Auth0)
    async checkForSession(){
        const session = request.only(['roken'])
            if(!session.token){
                return response.status(401).json({ data: 'Authentication Error' })
            }
            else{
                const user = await User.findBy({ token: session.token })
                    if(!user) return response.status(422).json({data: 'User Not Found'}); 
                    return response.status(200).json(user);
            }
    }

    async deleteUser(){
        const user = request.only(['token', 'id']);
        if(!user.token || !user.id) return response.status(422).json('Error with parameters. Please try again');
            const users = findBy({ token: user.token, id: user.id })
            if(!users) return response.status(404).json('Resource not found')
        
        await users.delete()
        return response.status(200).json('User delete successfully');
    }

    // simple register function, note that this still needs alot of improvements such as tokenization and other needed datas
    async register({params, request, response}) {
        const userDetails = request.only(['email', 'password', 'fullName', 'mobileNumber', 'image', 'background', 'address']);
        if(!userDetails.email || !userDetails.fullName || 
           !userDetails.mobileNumber || !userDetails.password || 
           !userDetails.address) { return response.status(422).json({message: 'Please fill up all required fields.'}) }

            const humbleTokenCode = "HMBLEBNDLE0000"
            const userToken = humbleTokenCode + Math.floor(Math.random() * 9999);
            const userImage = !userDetails.image ? 'https://apsec.iafor.org/wp-content/uploads/sites/37/2017/02/IAFOR-Blank-Avatar-Image.jpg' : userDetails.image;
            const userBackground = !userDetails.background ? 'https://c4.wallpaperflare.com/wallpaper/500/442/354/outrun-vaporwave-hd-wallpaper-preview.jpg' : userDetails.background;
     
            const user = new User()
            user.fullName = userDetails.fullName
            user.token = userToken
            user.image = userImage
            user.background = userBackground
            user.password = userDetails.password
            user.mobilenumber = userDetails.mobileNumber
            user.address = userDetails.address
            user.email = userDetails.email

            await user.save()
            return response.status(200).json({user})
    }
}

module.exports = UserController
