'use strict'

const { findBy } = require("../../Models/Game")

const Game = use('App/Models/Game')
class GameController {
    //base url for games lists
    async index ({response}) {
        let games = await Game.all()
        return response.json(games)
      }
      

    // games lists
      async getGameData ({params, response}) {
        const gameData = await Game.find(params.id)

        return response.json(gameData)
      }

    
    // delete a game function
      async delete({params,response, request}){
        const game = request.only(['id'])
          if(!game.id) return response.status(404).json('Invalid game or not found');
        const games = await findBy({ id: game.id });
          if(!games) return response.status(422).json('Something went wrong while deleting the game.')
        
          await games.delete()
          return response.status(200).json('Deleted Successfully')
      }  

}

module.exports = GameController
